console.log('planed lessons');
//Task-4*

//Display current user name on the page
const activeTeacherObj = JSON.parse(localStorage.getItem('teacher'));
const activeTeacherName = activeTeacherObj.name;
const activeTeacherEmail = activeTeacherObj.email;

console.log(activeTeacherName);

document.querySelector('#userName').textContent = activeTeacherName;
document.getElementById('userName').textContent = activeTeacherName;

console.log(document.querySelector('#userName').textContent);

document.querySelector('#userEmail').textContent = activeTeacherEmail;

document.querySelector(
  '.header-title'
).innerHTML = `<span id="headerTitle">${activeTeacherName}</span>" 👋"`;

const lessonsBlock = document.querySelector('.block__scheduled-lessons');

const addedLessons = document.querySelectorAll('div.card-box');

addedLessons.forEach((el) => el.remove());

let bookedLessonsArray = JSON.parse(localStorage.getItem('lessons'));

let bookedLessonsObj = JSON.parse(localStorage.getItem('lessons'));

const getLesson = (lesson) => {
  const { name, time, tomorrow, title, duration } = lesson;

  let startTime = time * 60;
  let endTime = (startTime + duration) / 60;

  let startTimeString = `${time.toString()}:00`;
  let endTimeString = `${Math.floor(endTime).toString()}:${
    endTime % 1 !== 0 ? (((endTime % 1) * 100 * 60) / 100).toString() : '00'
  }`;

  return `
<div class="card-box">
    <div class="card-illustration">
        <img src="./images/user_02.png" alt="">
    </div>
    <div class="info">
        <p class="sub-title">${
          tomorrow === true ? 'Завтра' : 'Сегодня'
        }, ${startTimeString} — ${endTimeString}</p>
        <p class="info-title">${name}</p>
        <p class="info-desc">${title}</p>
    </div>
</div>`;
};

const generateLessonsHTML = () => {
  const lessonsHTML = bookedLessonsObj.map((lesson) => {
    return getLesson(lesson);
  });

  lessonsBlock.insertAdjacentHTML('afterend', lessonsHTML);
};

if (localStorage.getItem('lessons')) {
  generateLessonsHTML();
} else {
  const noLessonsEl = `<p id="planedLessons" class="title" style="color:gray">Запланированых уроков нет</p>`;
  lessonsBlock.insertAdjacentHTML('beforeend', noLessonsEl);
}
