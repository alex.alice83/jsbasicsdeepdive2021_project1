import '../../../css/style.css';

import './reviews';
import './profile';
import './lessons';

const logoutBtn = document.getElementById('logoutBtn');

logoutBtn.onclick = () => {
  localStorage.removeItem('login');
  window.location.href = 'index.html';
};
