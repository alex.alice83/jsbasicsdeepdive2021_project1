import { saveToStorage } from './login';

console.log('wizard');

//Go to step selection of student or teacher

//Registration button
const regBtn = document.getElementById('regBtn');

//login block
const loginBlock = document.getElementById('loginBlock');
//step1Block
const step1Block = document.getElementById('step1Block');

//Event listener onclick on registration button
regBtn.addEventListener('click', () => {
  if (loginBlock.hasAttribute('style')) {
    loginBlock.removeAttribute('style');
    loginBlock.setAttribute('style', 'display: none');
    step1Block.setAttribute('style', 'display: flex');
  }
});

//Go to step Register

//Continue button
const toStep2Btn = document.getElementById('toStep2Btn');
const from1to2 = document.getElementById('from1to2');

//step2Block
const regBlock = document.getElementById('regBlock');

//Event listiner on proceed button
const goToRegisterStep = () => {
  if (step1Block.hasAttribute('style')) {
    step1Block.removeAttribute('style');
    step1Block.setAttribute('style', 'display: none');
    regBlock.setAttribute('style', 'display: flex');
  }
};

toStep2Btn.addEventListener('click', goToRegisterStep);
from1to2.addEventListener('click', goToRegisterStep);

//Go back action
//Goback buttons
const goBackPaginBtn = document.getElementById('from2to1');
const from3to2Svg = document.getElementById('from3to2Svg');
const toLoginSvg = document.getElementById('toLoginSvg');

const goBacToSelectkAction = () => {
  if (regBlock.hasAttribute('style')) {
    regBlock.removeAttribute('style');
    regBlock.setAttribute('style', 'display: none');
    step1Block.setAttribute('style', 'display: flex');
  }
};

const goBacTologin = () => {
  if (step1Block.hasAttribute('style')) {
    step1Block.removeAttribute('style');
    step1Block.setAttribute('style', 'display: none');
    loginBlock.setAttribute('style', 'display: flex');
  }
};

goBackPaginBtn.addEventListener('click', goBacToSelectkAction);
from3to2Svg.addEventListener('click', goBacToSelectkAction);
toLoginSvg.addEventListener('click', goBacTologin);

// Task-2.
// Storing studen / teache to the user object
// console.log(userTypeSelect[0].checked);

const user = {};

const addUserObject = () => {
  let selectedValue;
  const userTypeSelect = document.querySelectorAll('input[name="user-type"]');
  for (const iterator of userTypeSelect) {
    // console.log(iterator);
    if (iterator.checked) {
      // console.log(iterator.id);
      user.type = iterator.id.slice(5);
      // console.log(user);
    }
  }
};

toStep2Btn.addEventListener('click', addUserObject);
console.log(user);

//Create account button
const createAccountBtn = document.getElementById('createAccount');

let studentUsersArray = [];

const userCreateAccountCredentials = () => {
  //Form validation
  const password = document.getElementById('password');
  const password_second = document.getElementById('password_next');
  const fullNameId = document.getElementById('name');
  let formData = new FormData(regForm);
  let fullName = formData.get('name');
  console.log(fullName.split(' ').length);
  console.log(fullName.split(' ')[0]);
  console.log(fullName.split(' ')[0].length);

  if (
    fullName.split(' ').length < 2 ||
    fullName.split(' ')[0].length < 3 ||
    fullName.split(' ')[1].length < 3
  ) {
    fullNameId.classList.add('error');
    fullNameId.classList.add('error');
  } else if (password.value !== password_second.value) {
    password.classList.add('error');
    password_second.classList.add('error');
  } else {
    fullNameId.classList.remove('error');
    password.classList.remove('error');
    password_second.classList.remove('error');
    user.name = formData.get('name');
    user.email = formData.get('email');
    user.password = formData.get('password');
    // console.log(user);

    if (user.type === 'teacher') {
      if (!localStorage.getItem('teacher')) {
        localStorage.setItem('teacher', JSON.stringify(user));
      } else {
        localStorage.removeItem('teacher');
        localStorage.setItem('teacher', JSON.stringify(user));
      }
    } else {
      if (localStorage.getItem('students') === null) {
        studentUsersArray.push(user);

        localStorage.setItem('students', JSON.stringify(studentUsersArray));
      } else {
        studentUsersArray = JSON.parse(localStorage.getItem('students'));
        studentUsersArray.push(user);

        localStorage.setItem('students', JSON.stringify(studentUsersArray));
      }
    }
    //clear form
    document.getElementById('name').value = null;
    document.getElementById('email').value = null;
    document.getElementById('password').value = null;
    document.getElementById('password_next').value = null;
    //redirect
    window.location.href =
      user.type === 'teacher' ? 'teacher.html' : 'student.html';
  }
};

createAccountBtn.addEventListener('click', userCreateAccountCredentials);
