console.log('lessons form');
//Task-3
import { timeSlots, lessons } from './constants.js';
console.log(timeSlots.time_01);
console.log(lessons.type_01.title);

//Display current user name on the page
const activeUserArray = JSON.parse(localStorage.getItem('students'));
const activeUser = activeUserArray[activeUserArray.length - 1].name;

const activeUserEmail = activeUserArray[activeUserArray.length - 1].email;

document.querySelector('#profileName').textContent = activeUser;
document.querySelector('#profileEmail').textContent = activeUserEmail;
document.querySelector('#greeting').textContent = activeUser;

//Redaing current setting from radio buttons
let chekedLesssontype = document.querySelector(
  'input[name="type"][type="radio"]:checked'
).id;

const lessonType = document.querySelector('.block-types');

lessonType.addEventListener('click', () => {
  chekedLesssontype = event.target.id;
  console.log(chekedLesssontype);
});

let checkedLessonTimeSlot = document.querySelector(
  'input[name="time"][type="radio"]:checked'
).id;
console.log(checkedLessonTimeSlot);
const timeSlot = document.querySelector('.block__slot');
console.log(timeSlot);

let tomorrow =
  checkedLessonTimeSlot === 'time_01' || 'time_02' || 'time_03' ? false : true;

timeSlot.addEventListener('click', () => {
  checkedLessonTimeSlot = event.target.id;
  tomorrow =
    checkedLessonTimeSlot === 'time_01' ||
    checkedLessonTimeSlot === 'time_02' ||
    checkedLessonTimeSlot === 'time_03'
      ? false
      : true;
});

let nameArray = JSON.parse(localStorage.getItem('students'));
let lastLoggedUserName = nameArray[nameArray.length - 1].name;

const LessonSlotObject = () => {
  return {
    name: lastLoggedUserName,
    time: timeSlots[checkedLessonTimeSlot],
    tomorrow: tomorrow,
    title: lessons[chekedLesssontype].title,
    duration: lessons[chekedLesssontype].duration,
  };
};

let lessonsArray = [];

const bookLessonBtn = document.querySelector('.button-wrapper>button.button');

bookLessonBtn.addEventListener('click', (evt) => {
  evt.preventDefault();
  if (!localStorage.getItem('lessons')) {
    lessonsArray.push(LessonSlotObject());
    localStorage.setItem('lessons', JSON.stringify(lessonsArray));
  } else {
    lessonsArray = [];
    lessonsArray = JSON.parse(localStorage.getItem('lessons'));
    lessonsArray.push(LessonSlotObject());
    localStorage.setItem('lessons', JSON.stringify(lessonsArray));
  }
});
